package desck.tk.taller1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class AddActivity extends AppCompatActivity {
    TextView value;
    Button save;
    EditText number;
    int result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        value = findViewById(R.id.valueAdd);
        save = findViewById(R.id.saveAdd);
        number = findViewById(R.id.numberAdd);

        Bundle bundle = getIntent().getExtras();
        result = Integer.parseInt(bundle.getString("value", "0"));
        value.setText("Value = " + result);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent returnIntent = getIntent();
                result += Integer.parseInt(number.getText().toString());
                returnIntent.putExtra("result", "" + result);
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            }
        });

    }
}

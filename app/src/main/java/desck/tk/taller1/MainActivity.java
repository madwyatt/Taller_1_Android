package desck.tk.taller1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    TextView value, message;
    Button add, sub;
    int myValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        add = findViewById(R.id.add);
        message = findViewById(R.id.textView);
        sub = findViewById(R.id.sub);
        value = findViewById(R.id.value);
        value.setText("value = 0");
        myValue = 0;

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(MainActivity.this, AddActivity.class);
                myIntent.putExtra("value", "" + myValue);
                startActivityForResult(myIntent, 1);

            }
        });
        sub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(MainActivity.this, SubActivity.class);
                myIntent.putExtra("value", "" + myValue);
                startActivityForResult(myIntent, 2);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                String result = data.getStringExtra("result");
                myValue = Integer.parseInt(result);
                value.setText("Value = " + myValue);
                if (myValue <= 0) message.setText("you have a debt");

            } else {
                Toast.makeText(this, "Back", Toast.LENGTH_SHORT).show();
            }
        } else if (requestCode == 2) {
            if (resultCode == Activity.RESULT_OK) {
                String result = data.getStringExtra("result");
                myValue = Integer.parseInt(result);
                value.setText("Value = " + myValue);
                if (myValue <= 0) message.setText("you have a debt");

            } else {
                Toast.makeText(this, "Back", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
